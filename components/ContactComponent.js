import React, { Component } from 'react';
import { Card, Icon } from 'react-native-elements';
import { Text, TouchableOpacity, Linking, View, ScrollView } from 'react-native';

class Contact extends Component{
    render() {
        return(
            <ScrollView>
                <Card>
                    <Card.Title>Información de contacto</Card.Title>
                    <Card.Divider/>
                    <Text style={{margin: 10}}>
                        Kaixo Mendizale!{'\n'}{'\n'}
                        Si quieres participar en las salidas de montaña que organizamos o quieres hacerte soci@ de Gaztaroa, puedes contactar con nosotros a través de diferentes medios. Puedes llamarnos por teléfono los jueves de las semanas que hay salida (de 20:00 a 21:00). También puedes ponerte en contacto con nosotros escribiendo un correo electrónico, o utilizando la aplicación de esta página web. Y además puedes seguirnos en Facebook.{'\n'}{'\n'}
                        Para lo que quieras, estamos a tu disposición!{'\n'}
                    </Text>
                    <TouchableOpacity
                        style={{ flexDirection: "row" }}
                        onPress={()=>{ Linking.openURL('tel:+34948277151'); }}
                    >
                        <Text style={{marginLeft: 10}}>Tel:</Text>
                        <Text style={{textAlign: "right", color: "blue"}}>+34 948 277151{'\n'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ flexDirection: "row" }}
                        onPress={()=>{ Linking.openURL('mailto:gaztaroa@gaztaroa.com'); }}
                    >
                        <Text style={{marginLeft: 10}}>Email:</Text>
                        <Text style={{textAlign: "right", color: "blue"}}>gaztaroa@gaztaroa.com{'\n'}</Text>
                    </TouchableOpacity>
                </Card>
                <Card>
                    <Card.Title>Redes sociales:</Card.Title>
                    <Card.Divider/>
                    <View style={{flex: 1, flexDirection:"row", alignContent:"center", alignItems: 'center', justifyContent: 'space-evenly', padding: '0.1%' ,paddingStart: '25%', paddingEnd: '25%'}}>
                        <Icon
                            raised
                            reverse
                            color='#4267B2'
                            type='font-awesome'
                            name={ 'facebook-official' }
                            onPress={() => Linking.openURL('https://www.facebook.com/Gaztaroa-Mendi-Taldea-884785551650534/')}
                        />
                        <Icon
                            raised
                            reverse
                            color={'#FF0000'}
                            type='font-awesome'
                            name={ 'youtube' }
                            onPress={() => Linking.openURL('https://www.youtube.com/')}
                        />
                        <Icon
                            raised
                            reverse
                            color='#1DA1F2'
                            type='font-awesome'
                            name={ 'twitter' }
                            onPress={() => Linking.openURL('https://twitter.com/')}
                        />
                        <Icon
                            raised
                            reverse
                            color='#C13584'
                            type='font-awesome'
                            name={ 'instagram' }
                            onPress={() => Linking.openURL('https://www.instagram.com/')}
                        />
                    </View>
                </Card>
                <Card>
                    <Card.Title>¿Sugerencias? ¡Escríbenos!</Card.Title>
                    <Card.Divider/>
                    <View style={{flex: 1, flexDirection:"row", alignContent:"center", alignItems: 'center', justifyContent: 'space-evenly', padding: '0.1%', paddingStart: '30%', paddingEnd: '30%'}}>
                        <Icon
                            raised
                            reverse
                            color='#4267B2'
                            type='font-awesome'
                            name={ 'envelope-open-o' }
                            size={20}
                            onPress={() => Linking.openURL('mailto:sugerencias@gaztaroa.com?subject=Sugerencia para la app Gaztaroa 😃&body=Buenas: \nTengo una sugerencia para la app Gaztaroa')}
                        />
                        <Icon
                            raised
                            reverse
                            color={'#FF0000'}
                            type='font-awesome'
                            name={ 'globe' }
                            size={20}
                            onPress={() => Linking.openURL('https://www.gaztaroa.es/contacto')}
                        />
                    </View>
                </Card>
            </ScrollView>
            );
    }
}

export default Contact;