import React from 'react';
import { colorGaztaroaOscuro } from './comon/comon';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';

export const ActivityIndicatorComp = () => {
    return(
        <View style={styles.indicadorView} >
            <ActivityIndicator size="large" color={colorGaztaroaOscuro} />
            <Text style={styles.indicadorText} >En proceso . . .</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    indicadorView: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1
    },
    indicadorText: {
      color: colorGaztaroaOscuro,
      fontSize: 14,
      fontWeight: 'bold'
    }
});