import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Card } from 'react-native-elements';
import { ListItem, Avatar } from 'react-native-elements';
import { Text, ScrollView } from 'react-native';
import { ActivityIndicatorComp } from './ActivityIndicatorComponent';

const mapStateToProps = state => {
    return {
        activitiesProps: state.activities
    }
}

function HistoryCard() {
        return(
            <Card>
                <Card.Title>Un poquito de historia</Card.Title>
                <Card.Divider/>
                <Text style={{margin: 10}}>
                El nacimiento del club de montaña Gaztaroa se remonta a la primavera de 1976 cuando jóvenes aficionados a la montaña y pertenecientes a un club juvenil decidieron crear la sección montañera de dicho club. Fueron unos comienzos duros debido sobre todo a la situación política de entonces. Gracias al esfuerzo económico de sus socios y socias se logró alquilar una bajera. Gaztaroa ya tenía su sede social.
                {'\n'}{'\n'}
                Desde aquí queremos hacer llegar nuestro agradecimiento a todos los montañeros y montañeras que alguna vez habéis pasado por el club aportando vuestro granito de arena.
                {'\n'}{'\n'}
                Gracias!
                </Text>
            </Card>
        );
}

class WhoWeAre extends Component {
    render() {
        const RenderActivityItem = ({item, index}) => {
            return (
                <ListItem
                    key={index}
                    bottomDivider>
                    <Avatar source={{ uri: item.imagen }} />
                    <ListItem.Content>
                        <ListItem.Title>{item.nombre}</ListItem.Title>
                        <ListItem.Subtitle>{item.descripcion}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
            );
        };

        if (this.props.activitiesProps.isLoading) {
            return(
                <ScrollView>
                    <HistoryCard />
                    <Card>
                        <Card.Title>"Actividades y recursos"</Card.Title>
                        <Card.Divider/>
                        <ActivityIndicatorComp />
                    </Card>
                </ScrollView>
                );
        } else if (this.props.activitiesProps.errMess) {
            return(
                <View>
                    <Text>{props.errMess}</Text>
                </View>
            );
        } else {
            return(
                <ScrollView>
                    <HistoryCard />
                    <Card>
                        <Card.Title>"Actividades y recursos"</Card.Title>
                        <Card.Divider/>
                        {/* <FlatList
                        data={this.state.activities}
                        renderItem={RenderActivityItem}
                        keyExtractor={item => item.id.toString()}
                        /> */}
                        {this.props.activitiesProps.activities.map((item, index) => ( //Para no usar FlatList porque salen Warnings (Ignorables) y errores
                            <RenderActivityItem key={index} item={item} index={index}/>
                        ))}
                    </Card>
                </ScrollView>
            );
        }
    }
}

export default connect(mapStateToProps)(WhoWeAre);