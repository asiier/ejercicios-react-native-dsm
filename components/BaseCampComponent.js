import Home from './HomeComponent';
import { connect } from 'react-redux';
import Constants from 'expo-constants';
import React, { Component } from 'react';
import Contact from './ContactComponent';
import WhoWeAre from './WhoWeAreComponent';
import Calendar from './CalendarComponent';
import { Icon } from 'react-native-elements';
import ExcursionDetails from './ExcursionDetailsComponent';
import { View, StyleSheet, Image, Text, Button } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { createStackNavigator } from '@react-navigation/stack';
import { colorGaztaroaOscuro, colorGaztaroaClaro} from './comon/comon';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import { fetchHeaders, fetchComments, fetchExcursions, fetchActivities } from '../redux/ActionCreators';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import { SignIn, SignUp } from './SignInComponent';
import { AuthContext } from '../firebase/auth-context';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const mapStateToProps = state => {
    return {
        headersProps: state.headers,
        commentsProps: state.comments,
        excursionsProps: state.excursions,
        activitiesProps: state.activities
    }
}
const mapDispatchToProps = dispatch => ({
    fetchHeaders: () => dispatch(fetchHeaders()),
    fetchComments: () => dispatch(fetchComments()),
    fetchExcursions: () => dispatch(fetchExcursions()),
    fetchActivities: () => dispatch(fetchActivities())
  })

let defaultScreenOptions = {
    headerMode: 'float',
    headerTintColor: '#fff',
    headerTitleAlign: 'center',
    headerTitleStyle: { color: '#fff' },
    headerStyle: { backgroundColor: colorGaztaroaOscuro },
    headerLeftContainerStyle: { paddingLeft: 20 },
    headerTitleContainerStyle: { paddingRight: 20 },
    }
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    drawerHeader: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: -22,
        height: 122,
        backgroundColor: colorGaztaroaOscuro,
    },
    drawerHeaderText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    drawerImage: {
        margin: 10,
        width: 80,
        height: 60
    }
});

function CustomDrawerContent(props) {
    return (
        <DrawerContentScrollView {...props}>
            <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
                <View style={styles.drawerHeader}>
                <View style={{flex:1}}>
                <Image source={require('./imagenes/logo.png')} style={styles.drawerImage} />
                </View>
                <View style={{flex: 2}}>
                    <Text style={styles.drawerHeaderText}> Gaztaroa</Text>
                </View>
                </View>
                <DrawerItemList {...props} />
            </SafeAreaView>
        </DrawerContentScrollView>
    );
}

function HomeNavigator({ navigation }){

    defaultScreenOptions.headerLeft = () => (<Icon name="bars" type='font-awesome' size={24} style={{ marginLeft: 10 }} color= 'white' onPress={ () => navigation.dispatch(DrawerActions.toggleDrawer()) }/>)

    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={defaultScreenOptions}>

        <Stack.Screen
            name="Home"
            component={Home}
            options={{
            title: 'Campo Base',
            }}/>
        </Stack.Navigator>
    );
}

function CalendarNavigator({ navigation }) {
    return (
      <Stack.Navigator
        initialRouteName="Calendar"
        screenOptions={defaultScreenOptions}>

        <Stack.Screen
          name="Calendar"
          component={Calendar}
          options={{
            title: 'Calendario Gaztaroa',
          }}/>
        <Stack.Screen
          name="ExcursionDetails"
          component={ExcursionDetails}
          options={{
              title: 'Detalle Excursión',
              headerLeft: () => (<Icon name="arrow-left" type='font-awesome' size={24} color= 'white'
              onPress={ () => navigation.navigate("Calendar") }/>),
        }}/>
      </Stack.Navigator>
    );
}

function ContactNavigator(){
    return (
        <Stack.Navigator
            initialRouteName="Contact"
            screenOptions={defaultScreenOptions}>

        <Stack.Screen
          name="Contact"
          component={Contact}
          options={{
            title: 'Contacto',
          }}/>
        </Stack.Navigator>
    );
}

function WhoWeAreNavigator(){
    return (
        <Stack.Navigator
            initialRouteName="WhoWeAre"
            screenOptions={defaultScreenOptions}>

        <Stack.Screen
          name="WhoWeAre"
          component={WhoWeAre}
          options={{
            title: 'Quiénes somos',
          }}/>
        </Stack.Navigator>
    );
}

function SignInNavigator({ navigation }) {
    return (
        <Stack.Navigator
            initialRouteName="SignIn"
            screenOptions={defaultScreenOptions}>

        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={{
            title: 'Sign In',
          }}/>
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{
            title: 'Sign Up',
            headerLeft: () => (<Icon name="arrow-left" type='font-awesome' size={24} color= 'white'
            onPress={ () => navigation.navigate("SignIn") }/>)
          }}/>
        </Stack.Navigator>
    );
}

function DrawerNavigator() {
    return (
        <AuthContext.Consumer>
            {({auth, signOut}) => (
                <Drawer.Navigator
                    initialRouteName="Home"
                    drawerContent={props => <CustomDrawerContent {...props} />}
                    screenOptions={{
                        headerShown: false,
                        drawerStyle: {backgroundColor: colorGaztaroaClaro}
                    }}>
                    <Drawer.Screen name="Campo Base" component={HomeNavigator}
                        options={{
                        drawerIcon: ({ tintColor }) => (
                                <Icon
                                name='home'
                                type='font-awesome'
                                size={24}
                                color={tintColor}
                                />
                            )
                        }}
                    />
                    <Drawer.Screen name="Calendario" component={CalendarNavigator}
                        options={{
                            drawerIcon: ({ tintColor }) => (
                                    <Icon
                                    name='calendar'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                    />
                                )
                            }}
                    />
                    <Drawer.Screen name="Contacto" component={ContactNavigator}
                        options={{
                            drawerIcon: ({ tintColor }) => (
                                    <Icon
                                    name='address-card'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                    />
                                )
                            }}
                    />
                    <Drawer.Screen name="Quíenes somos" component={WhoWeAreNavigator}
                        options={{
                            drawerIcon: ({ tintColor }) => (
                                    <Icon
                                    name='info-circle'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                    />
                                )
                            }}
                    />
                    { auth ?
                        <></>
                    :
                        <Drawer.Screen name="Sign In" component={SignInNavigator}
                            options={{
                                drawerIcon: ({ tintColor }) => (
                                    <Icon
                                    name='user'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                    />
                                )
                            }}
                        />
                    }
                </Drawer.Navigator>
            )}
        </AuthContext.Consumer>
    );
}
class BaseCamp extends Component {
    componentDidMount() {
        this.props.fetchHeaders();
        this.props.fetchComments();
        this.props.fetchExcursions();
        this.props.fetchActivities();
    }

    render(){
        return (
            <NavigationContainer>
                <View style={{flex:1, paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight }}>
                    <DrawerNavigator />
                </View>
            </NavigationContainer>
        );
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BaseCamp);