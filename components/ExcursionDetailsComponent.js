import { connect } from 'react-redux';
import React, { Component } from 'react';
import { colorGaztaroaOscuro } from './comon/comon'
import { postFavorite, postComment } from '../redux/ActionCreators';
import { Card, Icon, Rating, Input } from 'react-native-elements';
import { Text, View, ScrollView, StyleSheet, Modal, Pressable, Share } from 'react-native';
import * as FileSystem from 'expo-file-system';
import * as MediaLibrary from 'expo-media-library';
import * as Sharing from 'expo-sharing';
import { AuthContext } from '../firebase/auth-context';

const mapStateToProps = state => {
    return {
        excursionsProps: state.excursions,
        commentsProps: state.comments,
        favoritesProps: state.favorites,
    }
}

const mapDispatchToProps = dispatch => ({
    postFavorite: (excursionID, postAction) => dispatch(postFavorite(excursionID, postAction)),
    postComment: (excursionID, rating, author, comment) => dispatch(postComment(excursionID, rating, author, comment))
});

const defaultForm = {
    rating: 3,
    author: "",
    comment: "",
}

const styles = StyleSheet.create({
    modal: {
        margin: '7%',
        marginTop: '5%',
        justifyContent: 'center',
    },
    button: {
        color: '#f0f',
        marginTop: '5%',
    },
    text: {
        fontSize: 24,
        textAlign: 'center',
        color: colorGaztaroaOscuro
    },
});

function RenderExcursion(props) {

    const excursion = props.excursion;

    if (excursion != null) {
            return(
                <Card>
                    <Card.Title>{excursion.nombre}</Card.Title>
                    <Card.Divider></Card.Divider>
                    <Card.Image source={{ uri: excursion.imagen }}></Card.Image>
                    <Text style={{margin: 20}}>
                        {excursion.descripcion}
                    </Text>
                    <View style={{flex: 1, flexDirection:"row", alignContent:"center", alignItems: 'center', justifyContent: 'space-evenly', paddingStart: '25%', paddingEnd: '25%'}}>
                        <Icon
                            raised
                            reverse
                            color='#f50'
                            type='font-awesome'
                            name={ props.fav ? 'heart' : 'heart-o'}
                            onPress={() => props.onPressFav()}
                        />
                        <Icon
                            raised
                            reverse
                            color={colorGaztaroaOscuro}
                            type='font-awesome'
                            name={ 'pencil' }
                            onPress={() => props.onPressTogglePostCommentModal()}
                        />
                        <Icon
                            raised
                            reverse
                            color='#88D498'
                            type='font-awesome'
                            name={ 'download' }
                            onPress={() => props.onPressSave()}
                        />
                        <Icon
                            raised
                            reverse
                            color='#612940'
                            type='font-awesome'
                            name={ 'share-square-o' }
                            onPress={() => props.onPressShare()}
                        />
                    </View>
                </Card>
            );
        }
    else {
        return(<View></View>);
    }
}

function RenderComments(props) {
    const comments = props.comments;

    const RenderCommentItem = ({item, index}) => {
        return (
            <View key={index.toString()} style={{margin: 10}}>
                <Text style={{fontSize: 14}}>{item.comentario}</Text>
                <Text style={{fontSize: 12}}>{item.valoracion} Stars</Text>
                <Text style={{fontSize: 12}}>{'-- ' + item.autor + ', ' + item.dia}</Text>
            </View>
        );
    };

    return (
        <Card>
            <Card.Title>Comentarios</Card.Title>
            <Card.Divider/>
            {comments.map((item, index) => ( //Para no usar FlatList porque salen Warnings (Ignorables) y errores
                <RenderCommentItem key={index} item={item} index={index}/>
            ))}
        </Card>
    );
}

class ExcursionDetails extends Component{
    constructor(props) {
        super(props);
        this.state = {
            currentUri: '',
            isActiveModal: false,
            modalForm: { ...defaultForm }
        }

        this.viewShotRef= React.createRef();
    }

    toggleFav(excursionID){
        if(this.props.favoritesProps.includes(excursionID)){
            this.props.postFavorite(excursionID,'DELETE');
        }else{
            this.props.postFavorite(excursionID,'ADD');
        }
    }

    togglePostCommentModal(excursionID){
        this.setState({isActiveModal: !this.state.isActiveModal});
    }

    handleComment(excursionID){
        this.props.postComment(excursionID, this.state.modalForm.rating, this.state.modalForm.author, this.state.modalForm.comment);
    }

    resetStateForm(){
        this.setState({modalForm: { ...defaultForm }});
    }

    saveScreenshot = async (props) => {
        let uri = props.imagen;

        let fileUri = FileSystem.documentDirectory + `${props.nombre}.jpg`;
        FileSystem.downloadAsync(uri, fileUri)
            .then(async (fileUriDown) => {
                const permission = await MediaLibrary.requestPermissionsAsync();
                if (permission.granted) {
                    try {
                    const asset = await MediaLibrary.createAssetAsync(fileUri);
                    MediaLibrary.createAlbumAsync('Images', asset, false)
                        .then(() => {
                        console.log('File Saved Successfully!');
                        })
                        .catch(() => {
                        console.log('Error In Saving File!');
                        });
                    } catch (error) {
                    console.log(error);
                    }
                } else {
                    console.log('Need Storage permission to save file');
                }
            })
            .catch(error => {
                console.error(error);
            })
    }

    shareExcursion = async (props) => {
            let uri = props.imagen;
            // Downloading the file
            let fileUri = FileSystem.documentDirectory + `${props.nombre}.jpg`;
            FileSystem.downloadAsync(uri, fileUri)
            .then(async ({fileUriDown}) => {
                try {
                  const result = await Share.share({
                    title: '¡Checkea los detalles de esta excursion!',
                    message:
                      `¡Checkea los detalles de esta excursion!\nVamos a ${props.nombre}\n${props.descripcion}`,
                    });
                    await Sharing.shareAsync(fileUri);
                } catch (error) {
                  alert(error.message);
                }
            })
            .catch(error => {
                console.error(error);
            })
      };

    render(){
        const {excursionID} = this.props.route.params;

        return(
            <ScrollView>
                    <RenderExcursion
                        excursion={this.props.excursionsProps.excursions[+excursionID]}
                        fav={this.props.favoritesProps.some(el => el === excursionID)}
                        onPressFav={() => this.toggleFav(excursionID)}
                        onPressTogglePostCommentModal={() => this.togglePostCommentModal(excursionID)}
                        onPressSave={() => this.saveScreenshot(this.props.excursionsProps.excursions[+excursionID])}
                        onPressShare={() => this.shareExcursion(this.props.excursionsProps.excursions[+excursionID])}
                        />
                <RenderComments
                    comments={Object.values(this.props.commentsProps.comments).filter(
                        (comments) => comments.excursionId === excursionID)}
                />
                <Modal
                    transparent= {false}
                    animationType = {"slide"}
                    visible = {this.state.isActiveModal}
                    onDismiss = {() => {
                        this.togglePostCommentModal();
                        this.resetStateForm();}
                    }
                    onRequestClose = {() => {
                        this.togglePostCommentModal();
                        this.resetStateForm();}
                    }>
                    <View style = {styles.modal}>
                        <Rating
                            showRating fractions="{0}"
                            startingValue="{3}"
                            onFinishRating={rating => this.setState(prevState => ({
                                modalForm: {                   // object that we want to update
                                    ...prevState.modalForm,    // keep all other key-value pairs
                                    rating: rating       // update the value of specific key
                                }
                            }))}></Rating>
                        <Input
                            placeholder="Autor"
                            style={{marginLeft: '3%'}}
                            onChangeText={authorText => this.setState(prevState => ({
                                modalForm: {                   // object that we want to update
                                    ...prevState.modalForm,    // keep all other key-value pairs
                                    author: authorText       // update the value of specific key
                                }
                            }))}
                            leftIcon={ <Icon name='user-o' type='font-awesome' size={22} /> }
                        />
                        <Input
                            placeholder="Comentario"
                            style={{marginLeft: '3%'}}
                            onChangeText={commentText => this.setState(prevState => ({
                                modalForm: {                   // object that we want to update
                                    ...prevState.modalForm,    // keep all other key-value pairs
                                    comment: commentText       // update the value of specific key
                                }
                            }))}
                            leftIcon={ <Icon name='comment-o' type='font-awesome' size={22} /> }
                        />
                        <Pressable
                            onPress={() =>{
                                this.togglePostCommentModal();
                                this.handleComment(excursionID);
                                this.resetStateForm();}
                            }
                            style={styles.button}>
                            <Text style={styles.text}>ENVIAR</Text>
                        </Pressable>
                        <Pressable
                            onPress={() =>{
                                this.togglePostCommentModal();
                                this.resetStateForm();}
                            }
                            style={styles.button}>
                            <Text style={styles.text}>CANCELAR</Text>
                        </Pressable>
                    </View>
                </Modal>
            </ScrollView>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExcursionDetails);