import { connect } from 'react-redux'
import React, { Component } from 'react';
import { SafeAreaView, View, Text, FlatList } from 'react-native';
import { ListItem, Avatar } from 'react-native-elements';
import { ActivityIndicatorComp } from './ActivityIndicatorComponent';

const mapStateToProps = state => {
    return {
        excursionsProps: state.excursions
    }
}
class Calendar extends Component {
     render(){
        const { navigate } = this.props.navigation;

        const renderCalendarItem = ({item, index}) => {
        return (
            <ListItem
                key={index}
                bottomDivider
                onPress={() => navigate('ExcursionDetails', { excursionID: item.id })}>
                <Avatar source={{ uri: item.imagen }} />
                <ListItem.Content>
                    <ListItem.Title>{item.nombre}</ListItem.Title>
                    <ListItem.Subtitle>{item.descripcion}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            );
        };

        if (this.props.excursionsProps.isLoading) {
            return(
                <ActivityIndicatorComp />
                );
        }
        else if (this.props.excursionsProps.errMess) {
            return(
                <View>
                    <Text>{props.errMess}</Text>
                </View>
            );
        }
        else {
            return (
                <SafeAreaView>
                    <FlatList
                        data={this.props.excursionsProps.excursions}
                        renderItem={renderCalendarItem}
                        keyExtractor={item => item.id.toString()}
                    />
                </SafeAreaView>
            );
        }
    }
}

export default connect(mapStateToProps)(Calendar);