import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Card } from 'react-native-elements';
import { Text, ScrollView, View } from 'react-native';
import { ActivityIndicatorComp } from './ActivityIndicatorComponent';

const mapStateToProps = state => {
    return {
        excursionsProps: state.excursions,
        headersProps: state.headers,
        activitiesProps: state.activities
    }
}

function RenderItem(props) {
    const item = props.item;

    if (props.isLoading) {
        return(
            <ActivityIndicatorComp />
        );
    } else if (props.errMess) {
        return(
            <View>
                <Text>{props.errMess}</Text>
            </View>
        );
    }else {
        if (item != null) {
            return(
                <Card>
                    <Card.Title>{item.nombre}</Card.Title>
                    <Card.Divider/>
                    <Card.Image source={{ uri: item.imagen }}></Card.Image>
                    <Text style={{margin: 20}}>
                        {item.descripcion}
                    </Text>
                </Card>
            );
        } else {
            return(<View></View>);
        }
    }
}

class Home extends Component {
     render() {
        if (this.props.headersProps.headers.isLoading && this.props.excursionsProps.excursions.isLoading && this.props.activitiesProps.activities.isLoading) {
            return(
                <ActivityIndicatorComp />
            );
        }else {
            return(
                <ScrollView>
                    <RenderItem item={this.props.headersProps.headers.filter((header) => header.destacado)[0]} />
                    <RenderItem item={this.props.excursionsProps.excursions.filter((excursion) => excursion.destacado)[0]} />
                    <RenderItem item={this.props.activitiesProps.activities.filter((activity) => activity.destacado)[0]} />
                </ScrollView>
            );
        }
    }
}

export default connect(mapStateToProps)(Home);