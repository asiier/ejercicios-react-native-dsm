import React, { useState } from 'react';
import { SafeAreaView, Text, TextInput, Button, StyleSheet } from 'react-native';
import { colorGaztaroaOscuro } from './comon/comon';
import { AuthContext } from '../firebase/auth-context';

export function SignIn(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    return (
        <AuthContext.Consumer>
            {({signIn}) => (
                <SafeAreaView>
                    <TextInput
                        style={styles.input}
                        placeholder="Email"
                        value={email}
                        onChangeText={setEmail}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Password"
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry={true}
                    />
                    <Text style={styles.text} onPress={() => {props.navigation.navigate('SignUp')}}>No account yet? Sign Up now.</Text>
                    <Button
                        style={styles.button}
                        color={colorGaztaroaOscuro}
                        title="Sign In"
                        onPress={() => {signIn(email, password)}}
                    />
                </SafeAreaView>
            )}
        </AuthContext.Consumer>
    );
}

export function SignUp() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    return (
        <AuthContext.Consumer>
            {({signUp}) => (
                <SafeAreaView>
                    <TextInput
                        style={styles.input}
                        placeholder="Email"
                        value={email}
                        onChangeText={setEmail}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Password"
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry={true}
                    />
                    <Button
                        style={styles.button}
                        color={colorGaztaroaOscuro}
                        title="Sign Up"
                        onPress={() => {signUp(email, password)}}
                    />
                </SafeAreaView>
            )}
        </AuthContext.Consumer>
    );
}

const styles = StyleSheet.create({
    input: {
      margin: 10,
      padding: 10,
      borderWidth: 1
    },
    text: {
        margin: 12,
        textAlign:'center',
        color:'blue',
        textDecorationLine: 'underline'
    },
    button: {
        width: '50'
    }
});