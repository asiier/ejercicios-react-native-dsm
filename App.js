import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import BaseCamp from './components/BaseCampComponent';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/configureStore';
import { PersistGate } from 'redux-persist/integration/react'
import { AuthContext } from './firebase/auth-context';
import { signUp, signIn, signOut } from './firebase/Firebase';

const { store, persistor } = ConfigureStore();

export default function App() {
  const [auth, setAuth] = useState();

  const appSignUp = (email, password) => {
    signUp(email, password, setAuth);
  }

  const appSignIn = (email, password) => {
    signIn(email, password, setAuth);
  }

  const appSignOut = () => {
    signOut();
    setAuth(null);
  }

  return (
    <AuthContext.Provider value={{
        auth: auth,
        signUp: appSignUp,
        signIn: appSignIn,
        signOut: appSignOut
    }}>
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <View style={styles.container}>
          <BaseCamp/>
          <StatusBar style="auto" />
        </View>
      </PersistGate>
    </Provider>
    </AuthContext.Provider>
  );
}

const styles = StyleSheet.create({container: {flex: 1}});