import React from "react";

export const AuthContext = React.createContext({
    auth: null,
    signUp: () => {},
    signIn: () => {},
    signOut: () => {}
});