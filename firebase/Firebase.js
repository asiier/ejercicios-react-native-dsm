import { initializeApp } from "firebase/app";
import { getReactNativePersistence, initializeAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAIY2OjgLo4rXJF2Anm5bsLVttQ0xnYnoo",
  authDomain: "gaztaroa-app.firebaseapp.com",
  databaseURL: "https://gaztaroa-app-default-rtdb.firebaseio.com",
  projectId: "gaztaroa-app",
  storageBucket: "gaztaroa-app.appspot.com",
  messagingSenderId: "743577137992",
  appId: "1:743577137992:web:ac65ff58e9ce5ff08c397d"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Authentication
const auth = initializeAuth(app, { persistence: getReactNativePersistence(AsyncStorage) });

// Sign Up
export const signUp = (email, password, onSuccess=() => {}, onError=() => {}) => {
    createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        onSuccess(user);
    })
    .catch((error) => onError(error));
}

// Sign In
export const signIn = (email, password, onSuccess=() => {}, onError=() => {}) => {
    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        onSuccess(user);
    })
    .catch((error) => onError(error));
}

// Sign Out
export const signOut = (onSuccess=() => {}, onError=() => {}) => {
    auth.signOut();
}