import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../components/comon/comon';

// -------------------------------------------------
// comments
// -------------------------------------------------

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments.json')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)))
    .catch(error => dispatch(commentsFailed(error.message)));
};

export const commentsFailed = (errmess) => ({
    type: ActionTypes.COMMENTS_FAILED,
    payload: errmess
});

export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});

export const postComment = (excursionID, rating, author, commentStr) => (dispatch) => {
    let comment = {
        autor: author,
        comentario: commentStr,
        dia: (new Date()).toISOString(),
        excursionId: excursionID,
        valoracion: rating
    };

    return fetch(baseUrl + 'comments.json', {
        method: 'POST',
        body: JSON.stringify(comment)
    })
    .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
    .then(response => response.json())
    .then(res => dispatch(addComment(comment)))
    .catch(error => dispatch(commentFailed(error.message)));
};

export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});

export const commentFailed = (errmess) => ({
    type: ActionTypes.COMMENT_FAILED,
    payload: errmess
})

// -------------------------------------------------
// excursions
// -------------------------------------------------

export const fetchExcursions = () => (dispatch) => {

    dispatch(excursionsLoading());

    return fetch(baseUrl + 'excursions.json')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(excursions => dispatch(addExcursions(excursions)))
    .catch(error => dispatch(excursionsFailed(error.message)));
};

export const excursionsLoading = () => ({
    type: ActionTypes.EXCURSIONS_LOADING
});

export const excursionsFailed = (errmess) => ({
    type: ActionTypes.EXCURSIONS_FAILED,
    payload: errmess
});

export const addExcursions = (excursions) => ({
    type: ActionTypes.ADD_EXCURSIONS,
    payload: excursions
});

// -------------------------------------------------
// headers
// -------------------------------------------------

export const fetchHeaders = () => (dispatch) => {

    dispatch(headersLoading());

    return fetch(baseUrl + 'headers.json')
    .then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
    .then(response => response.json())
    .then(headers => dispatch(addHeaders(headers)))
    .catch(error => dispatch(headersFailed(error.message)));
};

export const headersLoading = () => ({
    type: ActionTypes.HEADERS_LOADING
});

export const headersFailed = (errmess) => ({
    type: ActionTypes.HEADERS_FAILED,
    payload: errmess
});

export const addHeaders = (headers) => ({
    type: ActionTypes.ADD_HEADERS,
    payload: headers
});

// -------------------------------------------------
// activities
// -------------------------------------------------

export const fetchActivities = () => (dispatch) => {

    dispatch(activitiesLoading());

    return fetch(baseUrl + 'activities.json')
    .then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
    .then(response => response.json())
    .then(activities => dispatch(addActivities(activities)))
    .catch(error => dispatch(activitiesFailed(error.message)));
};

export const activitiesLoading = () => ({
    type: ActionTypes.ACTIVITIES_LOADING
});

export const activitiesFailed = (errmess) => ({
    type: ActionTypes.ACTIVITIES_FAILED,
    payload: errmess
});

export const addActivities = (activities) => ({
    type: ActionTypes.ADD_ACTIVITIES,
    payload: activities
});

// -------------------------------------------------
// favorites
// -------------------------------------------------

export const postFavorite = (excursionID, postAction)  => (dispatch) => {
    setTimeout(() => {
        if(postAction === 'ADD') dispatch(addFavorite(excursionID));
        if(postAction === 'DELETE') dispatch(deleteFavorite(excursionID));
    }, 250);
};

export const addFavorite = (excursionID) => ({
    type: ActionTypes.ADD_FAVORITE,
    payload: excursionID
});

export const deleteFavorite = (excursionID) => ({
    type: ActionTypes.DELETE_FAVORITE,
    payload: excursionID
});