import * as ActionTypes from './ActionTypes';

export const favorites = (state = [], action) => { // Implemento solo con state = [] porque no hay forma de que funcione con state = {favorites: []}
    switch (action.type) {
        case ActionTypes.ADD_FAVORITE:
            if (state.some(el => el === action.payload)){
                return state;
            } else {
                return state.concat(action.payload);
            }

        case ActionTypes.DELETE_FAVORITE:
            state = state.filter((value) => { return value != action.payload });
            return state;

        default:
          return state;
      }
};