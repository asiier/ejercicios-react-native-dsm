import thunk from 'redux-thunk';
//import logger from 'redux-logger';
import { headers } from './headers';
import { comments } from './comments';
import { favorites } from './favorites';
import { activities } from './activities';
import { excursions } from './excursions';
import { persistStore, persistReducer } from 'redux-persist'
import { createStore, combineReducers, applyMiddleware } from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const ConfigureStore = () => {
    const persistConfig = {
        key: 'root',
        storage: AsyncStorage,
        whitelist: ['favorites']
    }
    const rootReducers = combineReducers({ excursions, comments, headers, activities, favorites });

    const persistedReducer = persistReducer(persistConfig, rootReducers);

    const store = createStore(
        persistedReducer,
        applyMiddleware(thunk)
        // applyMiddleware(thunk, logger)
    );

    let persistor = persistStore(store);

    return { store, persistor };
}