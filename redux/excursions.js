import * as ActionTypes from './ActionTypes';

export const excursions = (state = { isLoading: true,
                                 errMess: null,
                                 excursions:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_EXCURSIONS:
            return {...state, isLoading: false, errMess: null, excursions: action.payload};

        case ActionTypes.EXCURSIONS_LOADING:
            return {...state, isLoading: true, errMess: null, excursions: []}

        case ActionTypes.EXCURSIONS_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        default:
          return state;
      }
};