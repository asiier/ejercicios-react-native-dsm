# Ejercicios React Native - DSM

**Ejercicios del 1 al 11 y el Proyecto Final** sobre React Native de la asignatura de DSM del MUITEL.

En este documento solo se comentarán los aspectos relevantes de cada ejercicio. Por lo que no se explicará nada ya explicado en los guiones del ejercicio.

También se comentarán aquellos problemas encontrados en cada ejercicio, así como aspectos importantes no comentados en los guiones.

- [Ejercicios React Native - DSM](#ejercicios-react-native---dsm)
  - [Ejercicio 01: Primeros pasos en React Native](#ejercicio-01-primeros-pasos-en-react-native)
    - [Introducción](#introducción)
    - [Comentario](#comentario)
  - [Ejercicio 02: Componentes React Native](#ejercicio-02-componentes-react-native)
    - [Introducción](#introducción-1)
    - [Comentario](#comentario-1)
  - [Ejercicio 03: Componentes funcionales en React Native](#ejercicio-03-componentes-funcionales-en-react-native)
    - [Introducción](#introducción-2)
    - [Comentario](#comentario-2)
  - [Ejercicio 04: Stack Navigation](#ejercicio-04-stack-navigation)
    - [Introducción](#introducción-3)
    - [Comentario](#comentario-3)
  - [Ejercicio 05: Drawer Navigation](#ejercicio-05-drawer-navigation)
    - [Introducción](#introducción-4)
    - [Comentario](#comentario-4)
  - [Ejercicio 06: Ejercicio Componentes y Navegación](#ejercicio-06-ejercicio-componentes-y-navegación)
    - [Introducción](#introducción-5)
    - [Comentario](#comentario-5)
  - [Ejercicio 07: Botones o Iconos](#ejercicio-07-botones-o-iconos)
    - [Introducción](#introducción-6)
    - [Comentario](#comentario-6)
  - [Ejercicio 08: Servidor JSON](#ejercicio-08-servidor-json)
    - [Introducción](#introducción-7)
    - [Comentario](#comentario-7)
  - [Ejercicio 09: Redux y Thunk](#ejercicio-09-redux-y-thunk)
    - [Introducción](#introducción-8)
    - [Comentario](#comentario-8)
      - [Diferencias entre Flux y Redux](#diferencias-entre-flux-y-redux)
      - [Uso de Redux](#uso-de-redux)
      - [Flujo de Redux](#flujo-de-redux)
      - [Uso de *"Middleware"* (Thunk)](#uso-de-middleware-thunk)
      - [Referencias](#referencias)
  - [Ejercicio 10: Activity Indicator y addFavoritos](#ejercicio-10-activity-indicator-y-addfavoritos)
    - [Introducción](#introducción-9)
    - [Comentario](#comentario-9)
  - [Ejercicio 11: Ejercicio Formularios y Modals con Redux](#ejercicio-11-ejercicio-formularios-y-modals-con-redux)
    - [Introducción](#introducción-10)
    - [Comentario](#comentario-10)
  - [Proyecto Final](#proyecto-final)
    - [Evaluación](#evaluación)
    - [Comentario](#comentario-11)

## Ejercicio 01: Primeros pasos en React Native

---

*Commit 01: “Primeros pasos en React Native”*

### Introducción

> Una vez descritos los objetivos y metodología que seguiremos en esta segunda mitad de la asignatura, comenzaremos este capítulo configurando nuestro entorno de trabajo.
>
> A continuación, daremos nuestro primer paso en la construcción de la aplicación de Gaztaroa, aunque, por el momento, ésta carecerá de toda funcionalidad.
>
> De este modo, cuando finalice este capítulo dispondremos de una aplicación muy sencilla y habremos aprendido a manejar las herramientas de las que nos valdremos en las próximas semanas

### Comentario

Este primer ejercicio no tiene mayor dificultad que seguir el guion del mismo. No obstante, hay algunos aspectos que merece la pena destacar.

La versión de **Node.js** deberá ser inferior o igual a la **16.14.2 LTS**, es decir, no usar la versión beta 17.X.X, ya que da errores con la versión de Expo 5.3.1. Por norma general utilizar la versión estable de Node.

La versión de Yarn también es fundamental. Habrá que emplear la versión **1.22.18** también conocido como *Yarn Classic*. Se puede instalar simplemente usando ``npm install yarn``

## Ejercicio 02: Componentes React Native

---

*Commit 02: “Componentes React Native”*

### Introducción

> Una vez que ya hemos creado un primer esqueleto básico de una aplicación React Native, es hora de ir avanzando poco a poco en la construcción de la Aplicación Gaztaroa.
>
> En este caso, haremos uso de la clase Component de React para generar nuestros primeros componentes en React Native. Por lo tanto, los conceptos que ya hemos asimilado en la primera parte de la asignatura siguen vigentes a la hora de trabajar con aplicaciones React Native.
>
> Los diferentes elementos de nuestra aplicación se albergarán dentro de estos componentes, pero, a diferencia de lo que ocurre en React, dónde los elementos son elementos HTML, en el caso de React Native emplearemos elementos React Native dentro de dichos componentes.
>
> A posteriori, cuando se compile el proyecto, esos elementos React Native serán convertidos a elementos específicos de cada plataforma (iOS o Android), permitiendo de ese modo su ejecución en los dispositivos móviles. Además de eso, crearemos nuestros primeros elementos de una interfaz de usuario empleando las clases nativas de React Native y haremos uso de otros elementos de interfaz de usuario que importaremos de la librería React Native Elements.
>
> De este modo, cuando finalice este ejercicio habremos comprendido el funcionamiento básico de la clase Component en aplicaciones React Native y habremos creado los primeros elementos de la interfaz de usuario de la aplicación.

### Comentario

Este segundo ejercicio tampoco tiene mayor dificultad que la del seguir el guion de este. Los conceptos *(explorados en el guion)* que hay que tener claros son:

- Diferencias entre componentes funcionales (sin estado) y clases (pueden tener estado)
- Llamadas entre componentes y *nesting*  
- Traspaso de información entre componente dentro de otro con el uso de los ``props``.
- El uso de componentes de la librería *React Native*

No obstante, todos estos son conceptos ya explorados al usar React.

Como nota adicional, he decido cambiar el idioma de todos los componentes al inglés para ser consistente. Por lo que de aquí en adelante todos los componentes estarán en inglés, así como todas las variables o funciones dentro de ellos.

## Ejercicio 03: Componentes funcionales en React Native

---

*Commit 03: “Componentes funcionales en React Native”*

### Introducción

> Al igual que en React, en React Native también distinguimos entre dos tipos de componentes: los Class Components (o componentes de clase) y los Functional Components” (o componentes funcionales). La elección de uno u otro en cada momento depende del contexto y el objetivo que se desee lograr. Para repasar este concepto se recomienda, como siempre, la lectura la bibliografía de este capítulo.
>
> Aunque ya hemos utilizado un componente funcional en el capítulo anterior (Calendario), en este capítulo profundizaremos un poquito más en este concepto y veremos la forma en que los diferentes componentes en una aplicación React Native pueden comunicarse entre ellos.
>
> En este ejercicio:
>
> - Profundizaremos en el concepto anteriormente señalado.
> - Crearemos la primera interacción con el usuario a través de la interfaz de usuario,
> analizando la forma en que React Native permite el envío de parámetros entre componentes.
> - Emplearemos el componente Card de React-Native-Elements para renderizar información.

### Comentario

Volviendo a seguir el guion al pie de la letra no se encuentran mayores dificultadas más allá de la impuesta por mi mismo de traducir los nombres al inglés y asegurarme de que funcione todo bien. Esto viene bien para asegurarse que uno entiende las diferencias entre variables que pueden tener el mismo nombre.

Puntos a destacar:

- Uso de nuevos componentes de React Native
- Lógica interna para devolver un componente a renderizar dependiendo de la información transmitida al componente.
- El uso de las funciones ``filter`` y ``onPress``
  - ``Filter`` itera sobre los componentes de un *array* y los filtra mediante un operador.
  - ``OnPress`` realiza una acción al presionar el/los elementos con esta propiedad.

## Ejercicio 04: Stack Navigation

---

*Commit 04: “Stack Navigation”*

### Introducción

> Ha llegado el momento de añadir capacidades de navegación a nuestro proyecto. De este modo, seremos capaces de navegar entre los diferentes componentes de una aplicación mediante menús integrados en la aplicación.
>
> En concreto, lo que se desea conseguir en este capítulo es crear un menú de navegación entre el componente Calendario (que será el primero en cargarse cuando se lance la aplicación) y el componente DetalleExcursión que se renderiza al hacer click en uno de los ListItems de Calendario.
>
> En primer lugar, configuraremos la navegación mediante el componente Stack Navigation de React Native, el cual basa su funcionamiento en un esquema de navegación en pila, a través de una jerarquía entre las diferentes “vistas” (componentes) de nuestra aplicación.

### Comentario

En este ejercicio se tendrá que instalar una serie de paquetes, no obstante, en vez de usar varios comandos se podría reducir a un par:

- ``expo install react-native-gesture-handler react-native-reanimated react-native-screens @react-native-community/masked-view``
- ``yarn add @react-navigation/native @react-navigation/stack``

Y habrá un paquete que no se menciona, pero será necesario instalar ``expo install react-native-safe-area-context``

Los puntos a destacar de este ejercicio serán:

- El uso del ``NavigationContainer`` y ``StackNavigator`` que en conjunto permiten crear pantallas independientes (Actividades en Android nativo).
- Siguiendo con este concepto y mediante el uso de ``StackNavigator`` se puede *navegar* entre las diferentes pantallas y habilita también las acciones nativas del dispositivo mediante gesto o botón de volver a atrás. También se puede implementar un botón en la aplicación que tenga el mismo cometido, el de volver a la pantalla o actividad anterior.

## Ejercicio 05: Drawer Navigation

---

*Commit 05: “Drawer Navigation”*

### Introducción

> En este segundo ejercicio dedicado a la navegación en React Native, implementaremos un navegador de “drawer” o “deslizante”.
>
> Además, se creará un nuevo componente que constituirá la página inicial de nuestra aplicación

### Comentario

En este nuevo ejercicio se indica la instalación de un paquete adicional ``yarn add @react-navigation/drawer`` así como añadir la línea ``plugins: ['react-native-reanimated/plugin'],`` al *return* del archivo **babel.config.js**.

Lo que no comenta el ejercicio y es necesario para volver a hacer funcionar a la app es la ejecución del comando ``--clear`` bien con el comando ```yarn start`` o ``expo run``. Esto limpiara la *cache*.

Los aspecto importante del ejercicio será el uso del paquete ``Drawer`` el cual permite la navegación entre las pantallas con el uso de una barra lateral. Para ello será necesario el uso del componente ``Drawer.Navigator``. Más allá de esos y de como se puede personalizar el *Drawer* (se explorá en el siguiente ejercicio) no hay mucho que comentar.

## Ejercicio 06: Ejercicio Componentes y Navegación

---

*Commit 06: “Ejercicio Componentes y Navegación”*

### Introducción

> En este ejercicio crearemos dos nuevos componentes y los integraremos en la aplicación. En concreto, se trata de un componente para mostrar una página de contacto y un segundo componente para comentar brevemente la historia del club de montaña y el tipo de actividades que realiza. En su diseño emplearemos FlatList, ListItem y el componente Card de React Native Elements, obteniendo parte de la información a mostrar de los ficheros de objetos JavaScript que ya hemos integrado en la aplicación.
> Además, será necesario modificar el Drawer menu y actualizar la navegación de la aplicación.

### Comentario

Este ejercicio tiene más chicha que comentar. Sobre todo porque habrá que crear desde *"cero"* dos nuevos componentes / pantallas. Y añadirlas al ``Navigator`` y a una de ellas pasarle información desde el componente base.

Empezando por el final, al componente de Campo Base (*BaseCampComponent.js*) se le han añadido las respectivas funciones *Navigator*, así como los componentes ``Drawer.Screens``. Ambos con sus respectivas opciones y apuntando a su respectivo componente previamente importado.

También se ha declarado una constante con las opciones por defecto (``screenOptions``) de los ``Stack.Navigator``, ya que se repetían para todos ellos. Esta decisión volverá más adelante para atormentarme.

Continuando por el componente de "Contacto" tendrá una implementación bastante simple tan solo usará los componentes de ``Card`` y ``Text``.

El componente de "Quiénes Somos" es más complejo. El componente ``HistoryCard`` será igual al componete "Contacto" antes creado. Haciendo empleo de los ``props`` se obtendrá la información de las actividades y mediante una ``ScrollView`` y una ``FlatList`` se representará esta. Se ha seguido el mismo enfoque que en el componente de "Calendario", el problema, es que al utilizar la ``ScrollView`` en conjunto con la  ``FlatList`` nos sale un warning.
Esto pasa en las nuevas versiones de React, ya que esos dos componentes tienes el mismo comportamiento (desplazamiento). Esta solución es completamente funcional y si desea desactivar el *"warning"*  se puede añadir a la clase las siguientes líneas

```javascript LogBox ignore
import { LogBox } from 'react-native';
    ...
    componentDidMount() {
        LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
    }
    ...
```

No obstante, esto será modificado en el próximo ejercicio por una mejor implementación que no da *"warnings"* ni dará problemas a la larga.

## Ejercicio 07: Botones o Iconos

---

*Commit 07: “Botones o Iconos”*

### Introducción

> En este capítulo vamos a implementar la que probablemente sea la forma más habitual de interactuar con el usuario: los botones. Sin embargo, vamos a hacerlo sin hacer uso del componente Button de React Native, ni de ninguna otra librería, ya que nos valdremos de Iconos para implementar dicha funcionalidad.
> Además, completaremos la vista de detalle de las excursiones, renderizando una nueva Card en la que se mostrarán los comentarios de los participantes de la excursión.

### Comentario

Como ya se introdujo en el [*"Ejercicio 05: Drawer Navigation"*](#ejercicio-05-drawer-navigation) la barra lateral (``Drawer``) puede ser personalizada. Por lo que haciendo uso del componente ``Icon`` se podrá añadir un icono específico para cada una de las entradas de la barra lateral. Así como un encabezado distintivo que contendrá el logo y nombre de la app. El cual se implementará empleando el parámetro ``drawerContent`` que llamará a una función que se ha creado (``CustomDrawerContent``) la cual devolverá los componentes pertinentes a renderizar.

Este problema de transferencia y guardado de información se hará cada vez más aparente cuanto más funcionalidad se quiere dar a la app. Hay muchas formas de solucionarlo, pero la idea es tener un sitio centralizado donde se puede leer y escribir información y todos los componentes tenga acceso a ella. Esto se puede conseguir con un servidor y llamadas a su API, una base de datos local o, lo que se explora en los próximos ejercicios, el uso de Redux.

El otro gran cambio realizado en este ejercicio es la adición de un icono para poder marcar como favorita una excursión. Este icono tendrá un evento ``onClick`` que cambiará el icono y lo añadirá a la lista de favoritos. No obstante, esta lista se trata una entrada en diccionario del estado de la clase ``ExcursionDetails`` esto quiere decir que los diferentes objetos creados con esa clase no compartirán la información, no solo eso, también se perderá el estado. Esto es debido a que cada vez que se acede a una excursión se genera una nueva instancia de la clase y en el constructor la lista está vacía y al no guardarse y transferirse la información de favorito, se pierde.

También se pide añadir un nuevo conjunto de información a los detalles de la excursión. Se tratan de los comentarios. Se puede abordar esto de la misma forma que se ha hecho hasta ahora, usando una ``FlatList``, y así se sugiere en el guion. No obstante, y como ya se indicó en anterior ejercicio, esto levanta un *"warning"*. No únicamente eso, sino que en este caso y al contrario del anterior ejercicio también levanta un error. Esto puede deberse a declarar el ``RenderCommentItem`` anidado dentro de la ``FlatList`` y todo ello en vuelto en la función ``RenderComments``. No obstante, no estoy seguro de que este sea el motivo y habría que investigar los *"logs"* del error.

Pero lo que si es seguro es la explicación dada en el [ejercicio 06](#comentario-5) por lo que no usar ``FlatList`` dentro de una ``ScrollView``. La forma por tanto de evitar el uso de ``FlatList`` dentro de ``ScrollViews`` será haciendo uso la función ``map``. De modo que se puede reemplazar la ``FlatList`` de esta forma:

```javascript Reemplazo de FlatList.
    return (
        <Card>
            <Card.Title>Comentarios</Card.Title>
            <Card.Divider/>
            {comments.map((item, index) => ( //Para no usar FlatList porque salen Warnings (Ignorables) y errores
                <RenderCommentItem key={index} item={item} index={index}/>
            ))}
        </Card>
    );
```

versus el original

```javascript Usando FlatList.
    return ( 
      <Card>
        <Card.Title>Comentarios</Card.Title>
        <Card.Divider/>
        <FlatList
            data={comentarios}
            renderItem={renderCommentarioItem}
            keyExtractor={item => item.id.toString()}
            />
      </Card>
    );
```

También se ha modificado para la pantalla de "Quiénes Somos" como se indicaba en el anterior ejercicio. Lo único a denotar de esta implementación es la importancia de que cada ``RenderCommentItem`` tenga una *"key"* diferente. Yo empleo el propio índice que devuelve la función ``map``, pero podría utilizarse el ``item.id``, ya que cada ítem tiene un identificador diferente.

## Ejercicio 08: Servidor JSON

---

*Commit 08: “Servidor JSON”*

### Introducción

> Este ejercicio tiene como objetivo instalar un servidor REST API local que nos permita simular un servidor real, sin necesidad de complicarnos demasiado con la configuración del mismo.
>
> Emplearemos dicho servidor para obtener toda la información que hasta ahora recabábamos de ficheros JavaScript locales. De este modo, los siguientes ficheros ya no serán necesarios en nuestra aplicación:
>
> - comun/excursiones.js
> - comun/cabeceras.js
> - comun/actividades.js
> - comun/comentarios.js
>
> Todos ellos, serán sustituidos por el archivo db.json que se encuentra adjunto a este ejercicio y en el que podemos encontrar la misma información. En todo caso, esto es algo que haremos en el siguiente ejercicio, por lo que no debéis borrarlos todavía!
>
> Además, el servidor JSON se encargará también de almacenar y enviar las imágenes que hasta ahora obteníamos desde la carpeta “componentes/imagenes”. A partir de ahora, esta carpeta únicamente almacenará el logotipo de Gaztaroa (logo.png), entendiendo que éste no varía con el tiempo y que, en consecuencia, merece la pena que se encuentre entre los ficheros nativos de los que hará uso la aplicación.

### Comentario

En este ejercicio se explorará una posible solución al problema planteado en el anterior ejercicio sobre la centralización de los datos.

El ejercicio es bastante simple, ya que tan solo habrá que modificar de donde se obtienen los datos y la manera de hacerlo.

Lo único reseñable que comentar es la constante necesidad de cambiar la IP del servidor, así como la entrada de la URL en el archivo ``comon.js``, siempre que se cambie de red LAN. Si se trabaja de manera local haciendo uso de un emulador es posible evitar estos cambios usando la IP *"loopback"* del ordenador (127.0.0.1), aunque depende de la configuración del emulador esto puede no funcionar. Si se tiene *"VirtualBox"* o *"VMware"* es muy probable que hayan creado un adaptador virtual para sus redes internas. Podemos aprovecharnos de esto poniendo nuestro servidor escuchando en cualquiera de las IPs del rango para tener así un servidor estático siempre accesible.

También hay que destacar que ahora se depende del servidor JSON para proveernos los archivos, por lo que el app no cargará sus pantallas si no puede realizar peticiones al servidor. Esto tiene sus pros y sus contras.

## Ejercicio 09: Redux y Thunk

---

*Commit 09: “Redux y Thunk”*

### Introducción

> Antes de continuar avanzando en el desarrollo de nuestra aplicación, añadimos un elemento que podría entenderse como una complicación en un entorno tan sencillo como el que vamos a manejar en esta asignatura, pero que nos será de gran ayuda para tener ordenada la aplicación: Redux.
>
> Redux es una implementación de la arquitectura Flux. Esto nos “obliga” a trabajar en una implementación unidireccional, lo que, a la postre, aportará predictibilidad y trazabilidad a la aplicación, haciendo más sencillo cualquier modificación que queramos implementar en la misma.
>
> Redux se basa en tres principios fundamentales:
>
> 1. Sólo hay una fuente de verdad.
> 2. El Estado es de solo lectura.
> 3. Los cambios se realizan con funciones puras.
>
> La siguiente figura describe gráficamente la arquitectura de trabajo Redux2 en la que, como podemos observar, el flujo de trabajo es unidireccional:
>
> ![Redux Workflow en React Native](./docs/capturasInforme/appGaztaroa_09_Redux_y_Thunk.jpg)
>
> Además de Redux, emplearemos el middleware Redux-Thunk, lo que nos permitirá realizar llamadas asíncronas a nuestra base de datos, y el middleware Redux-Logger para mostrar en consola los logs de la aplicación

### Comentario

Empezando por la ejecución del ejercicio siguiendo el guion del mismo no deja mucho margen al error. Habrá que asegurarse de incluir todos los archivos y hacer los cambios pertinentes a todas los archivos.

- Es importante el uso del componente ``Provider`` y el objeto *store* creado mediante la función ``ConfigureStore`` que será *"pasado"* a componente ``Provider`` en ``App.js``. El objeto *store* será inmutable y guardará toda la información (*state*) de la aplicación.
- Otro aspecto a destacar es la nueva forma de acceder a la información de cara a las diferentes pantallas / componentes. Estas necesitarán hacer uso de una función ``mapStateToProps`` donde se *mapeará* el ``state`` actual de la información de la aplicación al ``props`` individual del componente. (Esto lo consigue guardando un puntero que apunta hacia la posición de memoria donde está la información). Será también necesario el uso de la función ``connect`` al exportar el componente, la cual ejecutará la función ``mapStateToProps`` con el ``state`` que se le *pase* al componente.
- Por último, en nuestro componente principal ``BaseCampComponent.js`` será necesario una función adicional (ya que este es el encargado de distribuir y coordinar el resto de componentes). Esta función es ``mapDispatchToProps`` la cual será la encargada de modificar los datos de la aplicación. Esto lo realiza mediante la función ``dispatch`` que contendrá el *"payload"* (información) que modificar.

Eso era lo más relevante a la hora de efectuar la implementación en la app. Ahora se explicará la filosofía Flux y la implementación mediante Redux.

Existen debates sobre si técnicamente Redux es Flux o no. Digamos que tienen ciertos puntos en común y que Redux es un derivado de Flux. Pero existen ciertos conceptos que son la base de Redux y no se comparten con Flux.

#### Diferencias entre Flux y Redux

---

| Flux                                             | Ambas                                     | Redux                                  |
|--------------------------------------------------|-------------------------------------------|----------------------------------------|
| Multiples "Stores" (Multiples fuentes de verdad) | Acciones y creadores de acciones          | "Store" único (Unica fuente de verdad) |
| "Dispacher" separados                            | Flujo de datos unidireccional             | El "Dispatcher" es parte del "Store"   |
| "Reducers" opcionales                            | Fácil implementación (Cuestionable)        | Uso obligatorio de "Reducers"          |
| Precursor de Redux                               | No enlazada al componente / pantalla      | Evolución de Flux                      |
|                                                  | Tamaño reducido (Frente otras soluciones) |                                        |
|                                                  | Posibilidad de usar "Reducers"                         |                                        |

#### Uso de Redux

---

![Redux Flow](./docs/capturasInforme/WithRedux.png)
Fig. 1 - Diferencias entre no usar y usar Redux. [1]

En la figura 1 se puede ver la clara diferencia entre el uso de Redux. Al no usar Redux, los datos están **descentralizados** entre los diferentes componentes en sus ``states`` y ``props``. Al contrario, al usar Redux todos los datos estarán centralizados en el ``Store``.

Esto tiene la ventaja de tener un único sitio de donde obtener la información y a donde escribirla. La implementación inicial puede tomar su tiempo frente a no utilizarlo, pero a la larga se ahorrará tiempo al utilizarlo. Sobre todo en aplicaciones que tenga muchos datos junto a muchos componentes que accedan a dichos datos. Para aplicaciones pequeñas es más cuestionable su utilidad.

#### Flujo de Redux

---

![Redux Flow](./docs/capturasInforme/Redux.png)
Fig. 2 - Flujo Redux

Los puntos más importantes a entender de la figura 2 es la unidireccionalidad del flujo de los datos.

1. Se obtendrá la información del ``Store`` por medio del ``Subscribe`` (en nuestro caso, ``mapStateToProps`` y ``fetch...`` Ej. ``fetchComments``). Y se *"mapeará"* al ``props`` individual del componente.
2. Para añadir / borrar / actualizar los datos se hará por medio del ``Dispatcher``. Que irán a las ``Actions``.
3. Dependiendo del tipo de acción se realizarán ciertos comandos y se pasará el *"payload"* (Nuevo estado) junto al tipo de acción al ``Reducer``.
4. El ``Reducer`` (en nuestro caso, ``./redux/....js`` Ej. ``comments.js``) creará un nuevo objeto con los datos actualizados que se guardaran en el ``Store`` para empezar de nuevo el proceso. En el caso de Redux los ``Reducers`` se combinarán en el ``Store`` obteniendo así un único conjunto de ``Reducers``, un único Dispatcher y una única fuente de verdad.

#### Uso de *"Middleware"* (Thunk)

---

El uso de un *"Middleware"* no es siempre necesario. En este caso se usará el *"middleware"* llamado Thunk. Thunk permitirá que los ``ActionCreators.js`` puedan devolver funciones y no tan solo objetos JS. Esto, por tanto, permite que se empleen peticiones asíncronas a APIs. En nuestro caso será las peticiones a la API REST que nos proporciona el servidor JSON.

![Redux Flow](./docs/capturasInforme/ReduxFlow.gif)
Fig. 3 - Flujo de Redux animado con el empleo del *"middleware"* Thunk y una API asíncrona. [2]

#### Referencias

- [1] [Leveling Up with React: Redux](https://css-tricks.com/learning-react-redux/)
- [2] [Web development project 4 (IT2810)](https://github.com/skykanin/webdev-project-group38#client-third-party-libraries-and-components=)

## Ejercicio 10: Activity Indicator y addFavoritos

---

*Commit 10: “Activity Indicator y addFavoritos”*

### Introducción

> En  este  ejercicio  avanzaremos  en  nuestra  aplicación,  dotándola  de  la  capacidad  de modificar  el  estado  del  Store  para almacenar las excursiones “favoritas” del usuario. Además, intentaremos mejorar la experiencia de usuario, añadiendo información sobre  el estado de ejecución de la aplicación, mediante un sencillo loader.

### Comentario

La implementación del indicador de actividad (``ActivityIndicator``) no se desvía de lo explicado en el guion. Haciendo uso de ``if`` y ``else if`` se comprobará que componente habrá que devolver dependiendo del estado de la petición.

Una forma de verificar si esta funcionalidad está funcionando de forma correcta puede ser desactivando el *"JSON Server"* de manera que la petición se quede procesando (y, con ello, aparezca el ``ActivityIndicator``) hasta que ocurra el *"timeout"* resultando en el mensaje de error.

La implementación de los favoritos sí que se ha desviado un poco del guion. En vez de emplear el sugerido Array dentro de un diccionario sea, ha decidido utilizar un simple Array. Esto simplifica el código a expensas de romper la "consistencia" seguida en el resto de la app para almacenar los datos dentro de diccionarios, siendo la *"key"* el tipo de valor almacenado. (Ej.: ``{"excursiones": [...]}``)

## Ejercicio 11: Ejercicio Formularios y Modals con Redux

---

*Commit 11: “Ejercicio Formularios y Modals con Redux”*

### Introducción

> En este segundo ejercicio no-guiado pondremos en práctica los conocimientos adquiridos en relación a Redux, para crear un formulario que permita al usuario de la aplicación añadir comentarios en relación a las excursiones de Gaztaroa.
>
> Para ello, además de renderizar los componentes de la interfaz gráfica que nos permitan introducir los datos requeridos en el formulario, deberemos actualizar nuestro repositorio  Redux  de  tal  forma  que  los  comentarios  introducidos  se  almacenen  en  el  estado  de  la  aplicación.  Nótese  que  no  pretendemos  modificar  la  información  que  se  almacena en el servidor. Eso lo dejamos para más adelante.
>
> Para mayor claridad, dividimos los requerimientos de este ejercicio en dos apartados, centrando el primero en la interfaz gráfica y dejando para el segundo el enlace de dicha  interfaz con Redux.

### Comentario

En este último ejercicio se nos da más libertad (o no tantas pistas) a la hora de realizar su implementación. No obstante, se procederá de una forma parecida a como se viene haciendo hasta ahora.

Para no alargarnos en las explicaciones trataremos de comentar solo los aspectos más relevantes.

Para el ``Modal`` se ha tratado de imitar el diseño visible en el guion. Algo potencialmente significativo es que se ha decidido optar por el componente ``Pressable`` para efectuar los botones de "ENVIAR" y "CANCELAR". En oposición a usar únicamente texto o el botón nativo, ya que estos tienen sus desventajas (funcionalidad y visuales). ``Pressable`` es una buena unión entre ambos. Se ha utilizado ``Input`` para los recuadros de texto, puesto que era la manera más sencilla de colocar un icono en la parte izquierda. Este tiene sus desventajas de funcionalidad, sobre todo si es empleado en formularios de usuario y contraseña, pero para simple texto es suficiente.

Para la construcción del formulario que se enviará para la adición de comentarios se ha optado por no incluir el día dentro del state del componente y añadirlo dentro de la función ``postComment()`` del ``ActionCreators.js``. También se han condensado el autor, valoración y comentario en un diccionario dentro de ``state`` con nombre de *"key"* modalForm. Por temas de orden y consistencia.

Por último, para calcular el ID que tendrá el nuevo comentario tan solo se comprobará la longitud del array existente de comentarios y ese será el ID. Por lo que estarán ordenados por orden cronológico usando el propio ID. Esto podrá ser un problema más adelante cuando se emplee una base de datos compartida por más de un usuario y a la que, potencialmente, podrían escribir 2 o más personas a la vez. Sin embargo, ahora mismo los datos son locales, ni siquiera están siendo escritos a la base de datos del *"JSON Server"*, por lo que este enfoque es suficiente.

## Proyecto Final

- Backend en la nube: Firebase
- Acceso a capacidades nativas del terminal móvil (Expo SDK y React Native)
- Persistent storage
- Authentication

### Evaluación

> - [X] **Ejercicios guiados (4 puntos)** - Pabolleta y Montes
> - [X] Configurar backend Firebase para descarga de información (0.5 punto) - Pabolleta
>   - REST API + descarga imágenes
> - [X] Configurar backend Firebase upload: comentarios (0.5 puntos) - Pabolleta
> - [X] Interfaz de usuario avanzada y acceso a capacidades nativas (2 punto) - Montes
>   - 0.5 puntos por cada capacidad nativa integrada en la aplicación
>     - En esta categoría entraría la memoria persistente. No os obligatorio implementarlo.
>   - Animaciones o interfaces avanzadas: 0.5 puntos
> - [X] Autenticación (1.5 puntos)  - Pabolleta y Montes
>   - [X] Autenticación usuario en aplicación (0.5 puntos) - Pabolleta
>     - Gestor OAuth (Google, Facebook…)
>   - [ ] Gestionar aspectos de la aplicación a partir de dicha autenticación (1 puntos) - Montes
> - [X] **Gestión del proyecto y participación en clase (1.5 punto)** - Pabolleta y Montes

### Comentario

El comentario ha sido realizado en un documento aparte.

Puede acceder al mismo mediante este link: [**ProyectoFinal.md**](./docs/ProyectoFinal.md)
