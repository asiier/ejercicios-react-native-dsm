# Proyecto Final

## Índice

- [Proyecto Final](#proyecto-final)
  - [Índice](#índice)
  - [Gaztaroa App](#gaztaroa-app)
    - [Integración con Firebase](#integración-con-firebase)
    - [Publicación de comentarios](#publicación-de-comentarios)
    - [Autentificación](#autentificación)
    - [Características nativas](#características-nativas)
      - [**1. Almacenamiento local de 'Favoritos'**](#1-almacenamiento-local-de-favoritos)
      - [**2. Guardar imagen destacada de la excursión**](#2-guardar-imagen-destacada-de-la-excursión)
      - [**3. Compartir excursión**](#3-compartir-excursión)
      - [**4. Envío de correos / Redirección de links web / Apertura de número de teléfono**](#4-envío-de-correos--redirección-de-links-web--apertura-de-número-de-teléfono)

## Gaztaroa App

En la versión final de la app Gaztaroa se han añadido las siguientes funcionalidades:

### Integración con Firebase

- [Firebase Realtime Database](https://firebase.google.com/docs/database) para el almacenamiento en formato JSON de datos en la nube

  ![Firebase Realtime Database](./images/firebase-realtime.png)

- [Firebase Cloud Storage](https://firebase.google.com/docs/storage) para el almacenamiento de imágenes

  ![Firebase Storage](./images/firebase-storage.png)

- Se acceden a los recursos de Firebase mediante la [API REST](https://firebase.google.com/docs/database/rest/start)

### Publicación de comentarios

- Se ha integrado la funcionalidad de publicar comentarios en la aplicación

  ![Comments](./images/comments.png)

### Autentificación

Con el objetivo de autentificar al usuario y poder restringir las acciones del mismo sobre la app (peticiones a la API, ecritura en BBDD, etc.) se ha empleado la herramienta de autentificación proporcionada por Firebase, [Firebase Authentication](https://firebase.google.com/docs/auth).

- Firebase Authentication con SDK de Firebase 'firebase/auth'
- React `Context` para almacenar el objeto devuelto en la autentificación
- Formularios sign in/sign up para inicio de sesión y registro de nuevos usuarios

### Características nativas

La puntuación de este apartado es de 2 puntos los cuales se conseguían de la siguiente manera:
> - 0.5 puntos por cada capacidad nativa integrada en la aplicación
>   - En esta categoría entraría la memoria persistente. No os obligatorio
> - Animaciones o interfaces avanzadas: 0.5 puntos

Por lo que se ha decidido realizar la integración de 4 capacidades nativas para tratar de obtener la máxima puntuación en este apartado.

#### **1. Almacenamiento local de 'Favoritos'**

La primera de las capacidades nativas implementadas ha sido la memoria persistente (Persit Storage). La implementación solo se ha realizado para los favoritos dado que el resto de información está en Firebase y no tendría mucho sentido guardarlo de forma persistente (O al menos sin implementar un sistema para comprobar si ha habido cambios). Por tanto, únicamente los favoritos serán guardados.

Para esto se han empleado un par de paquetes: `` redux-persist, @react-native-async-storage/async-storage``.

El código no demasiado complejo.

En el componente ``configureStore.js`` se tendrá que generar un objeto ``persistReducer`` que tomará como argumentos un objeto de configuración y uno de ``combineReducers`` donde tendrán que ir todos los *"reducers"*.

En el objeto de configuración habrá que establecer la *"key"*, el *"storage"* y, opcionalmente, una *"whitelist"* o *"blacklist"*.

El objeto ``persistReducer`` será introducido al ``createStore`` para crear un objeto ``store``. De este se "extraerá" el ``persistor``. Y tanto el ``persitor`` como el ``store`` se devolverán

``` javascript
...
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage';

export const ConfigureStore = () => {
    const persistConfig = {
        key: 'root',
        storage: AsyncStorage,
        whitelist: ['favorites']
    }
    const rootReducers = combineReducers({ excursions, comments, headers, activities, favorites });

    const persistedReducer = persistReducer(persistConfig, rootReducers);

    const store = createStore(
        persistedReducer,
        applyMiddleware(thunk)
    );

    let persistor = persistStore(store);

    return { store, persistor };
}
```

Y en el componente ``App.js`` habrá que hacer uso de ese ``persistor`` mediante el ``PersistGate``.

``` javascript
...
import { PersistGate } from 'redux-persist/integration/react'

const { store, persistor } = ConfigureStore();

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
            ...
      </PersistGate>
    </Provider>
  );
}
```

De manera adicional también se ha decidido implementar la opción de eliminar una excursión de favoritos. Esto está implementando usan el botón de *"toggle"*.

Se ha creado una nueva acción en el ``ActionCreators.js`` y añadida la respectiva entrada en ``ActionTypes.js``. Dentro del componente ``favorites.js`` se ha implementado este nuevo caso de tal forma que elimine el id del *"payload"* del *"Array"* del ``state``.

**Video:**

![Almacenamiento local de 'Favoritos'](./videos/1.%20Almacenamiento%20local%20de%20'Favoritos'.mp4)
<figure class="video_container">
  <video width=300 controls="true" allowfullscreen="true" poster="./images/1. Almacenamiento local de 'Favoritos'.jpg">
    <source src="https://gitlab.com/asiier/ejercicios-react-native-dsm/-/raw/main/docs/videos/1.%20Almacenamiento%20local%20de%20'Favoritos'.mp4" type="video/mp4">
  </video>
</figure>

#### **2. Guardar imagen destacada de la excursión**

Esta funcionalidad nativa ha sido bastante tediosa. Pero ha servido para entender que es Expo y en que se diferencia en React Native CLI (O Bare React Native).

Expo se trata de un framework encima del propio framework que es React Native que modifica el flujo de trabajo de la app React tratando de hacerla más *"streamline"*. También, añade nuevas funcionalidades, sobre todo relacionadas con interactuar con las capacidades nativas del dispositivo. Es decir, mediante librerías mapea funcionalidades nativas interactuando con el SDK.

Este enfoque es muy atractivo para construir un app rápidamente y hacer cambios sin necesitar compilar y reinstalar dicha aplicación. No obstante, tiene algunos problemas o cosas a considerar. Al modificar el flujo de trabajo y básicamente como se construye la app, ciertos paquetes/librerías tienen conflictos y directamente no funcionan. Sobre todo son aquellos paquetes que interactúan con el SDK. Por lo que nos encontramos que muchos paquetes que integran capacidades nativas son incompatibles, dejándonos solo con las creadas por el equipo de Expo. Como es comprensible, esta funcionalidad es más limitada que la que podemos encontrar en el resto de la comunidad. Por lo que hacemos un *"trade-off"*  comodidad por flexibilidad.

Otro asunto que merece la pena comentar son los problemas asociados a usar un software en continuo desarrollo. En el cual, te puedes encontrar que de una versión para otra la forma de hacer las cosas ha cambiado completamente. Y tu código ahora está obsoleto o la documentación/tutoriales que puedes encontrar ya no sirven. Pero esto tampoco debería ser un gran problema, ya que es algo recurrente en la tecnología. Si te duermes te quedas atrás. Esto es un continuo aprendizaje con la meta de producir algo más eficiente, más fácil de utilizar, más estético, algo mejor.

Después de esta introducción veamos la implementación de las características.

Se hará uso de dos paquetes de la librería Expo: ``expo-file-system, expo-media-library``.

A dia de hoy, la manera descargar y guardar archivos es la siguiente:
Habrá descargar la imagen y guardarla localmente en la carpeta datos internos de la app a la cual solo tiene acceso dicha app.
Luego se tendrá que pedir permisos al usuario para acceder a galería mediante la ``MediaLibrary``.
Si los permisos son concedidos se podrá crear un album donde se guardará la imagen.

```javascript
...
import * as FileSystem from 'expo-file-system';
import * as MediaLibrary from 'expo-media-library';

  ...
    saveScreenshot = async (props) => {
        let uri = props.imagen;

        let fileUri = FileSystem.documentDirectory + `${props.nombre}.jpg`;
        FileSystem.downloadAsync(uri, fileUri)
            .then(async (fileUriDown) => {
                const permission = await MediaLibrary.requestPermissionsAsync();
                if (permission.granted) {
                    try {
                    const asset = await MediaLibrary.createAssetAsync(fileUri);
                    MediaLibrary.createAlbumAsync('Images', asset, false)
                        .then(() => {
                        console.log('File Saved Successfully!');
                        })
                        .catch(() => {
                        console.log('Error In Saving File!');
                        });
                    } catch (error) {
                    console.log(error);
                    }
                } else {
                    console.log('Need Storage permission to save file');
                }
            })
            .catch(error => {
                console.error(error);
            })
    }
    ...

```

**Video:**

![Guardar imagen destacada de la excursión](./videos/2.%20Guardar%20imagen%20destacada%20de%20la%20excursi%C3%B3n%20y%203.%20Compartir%20Excursi%C3%B3n.mp4)
<figure class="video_container">
  <video width=300 controls="true" allowfullscreen="true" poster="./images/2. Guardar imagen destacada de la excursión.jpg">
    <source src="https://gitlab.com/asiier/ejercicios-react-native-dsm/-/raw/main/docs/videos/2.%20Guardar%20imagen%20destacada%20de%20la%20excursi%C3%B3n%20y%203.%20Compartir%20Excursi%C3%B3n.mp4" type="video/mp4">
  </video>
</figure>

#### **3. Compartir excursión**

La idea para esta implementación de una característica nativa era la de poder compartir el nombre y descripción de una excursión junto a su imagen. Esto posible hacerlo mandado la imagen como texto en base64.

El problema es que tanto la implementación de ``Share`` de React Native como la de ``Sharing`` de Expo no permiten hacerlo. Existe un paquete que si permite hacerlo (react-native-share), pero no es compatible con Expo.

Por lo que se ha decidido (como prueba de concepto) enviar primero la imagen y luego el texto.

La implementación para descargar la imagen es igual que antes, pero ahora podremos mandar directamente la ``fileUri``. Usando la función ``share`` del objeto ``Share`` compartiremos el texto. Y con ``Sharing.shareAsync`` la imagen.

```javascript
...
import { Share } from 'react-native';
import * as Sharing from 'expo-sharing';

...
    shareExcursion = async (props) => {
            let uri = props.imagen;
            // Downloading the file
            let fileUri = FileSystem.documentDirectory + `${props.nombre}.jpg`;
            FileSystem.downloadAsync(uri, fileUri)
            .then(async ({fileUriDown}) => {
                try {
                  const result = await Share.share({
                    title: '¡Checkea los detalles de esta excursion!',
                    message:
                      `¡Checkea los detalles de esta excursion!\nVamos a ${props.nombre}\n${props.descripcion}`,
                    });
                    await Sharing.shareAsync(fileUri);
                } catch (error) {
                  alert(error.message);
                }
            })
            .catch(error => {
                console.error(error);
            })
      };
```

**Video:**

![Guardar imagen destacada de la excursión](./videos/2.%20Guardar%20imagen%20destacada%20de%20la%20excursi%C3%B3n%20y%203.%20Compartir%20Excursi%C3%B3n.mp4)
<figure class="video_container">
  <video width=300 controls="true" allowfullscreen="true" poster="./images/3. Compartir Excursión.jpg">
    <source src="https://gitlab.com/asiier/ejercicios-react-native-dsm/-/raw/main/docs/videos/2.%20Guardar%20imagen%20destacada%20de%20la%20excursi%C3%B3n%20y%203.%20Compartir%20Excursi%C3%B3n.mp4" type="video/mp4">
  </video>
</figure>

#### **4. Envío de correos / Redirección de links web / Apertura de número de teléfono**

Esta capacidad nativa tampoco tiene mayor complicación. Pero es una característica altamente utilizada hoy en dia por lo que creímos que sería una buena adicción.

Para ella no es necesario instalar ningún paquete adicional dado que está dentro del *"core"* de React Native.

Si se observa el código se comprueba la sencilla implementación.

```javascript
import { Linking, ... } from 'react-native';

class Contact extends Component{
            ...
                    <TouchableOpacity
                        style={{ flexDirection: "row" }}
                        onPress={()=>{ Linking.openURL('tel:+34948277151'); }}
                    >
                        <Text style={{marginLeft: 10}}>Tel:</Text>
                        <Text style={{textAlign: "right", color: "blue"}}>+34 948 277151{'\n'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ flexDirection: "row" }}
                        onPress={()=>{ Linking.openURL('mailto:gaztaroa@gaztaroa.com'); }}
                    >
                        <Text style={{marginLeft: 10}}>Email:</Text>
                        <Text style={{textAlign: "right", color: "blue"}}>gaztaroa@gaztaroa.com{'\n'}</Text>
                    </TouchableOpacity>
                    ...
                    <Icon
                        raised
                        reverse
                        color='#4267B2'
                        type='font-awesome'
                        name={ 'facebook-official' }
                        onPress={() => Linking.openURL('https://www.facebook.com/Gaztaroa-Mendi-Taldea-884785551650534/')}
                    />

```

Básicamente, se resume en emplear ``Linking`` con la función ``openURL`` a la cual se dará como argumento una *"Uniform Resource Identifier"* (URI). Dado que esta funcionalidad lleva presente muchos años en la web no hay que hacer ningún tipo configuración adicional. Entiende los protocolos ``mailto:`` y ``tel:`` y abre las aplicaciones predeterminadas para dicho protocolo del dispositivo usado. Cabe destacar que en el caso del mail se puede incluso añadir un encabezado y cuerpo del mail.

```js
() => Linking.openURL('mailto:sugerencias@gaztaroa.com?subject=Sugerencia para la app Gaztaroa 😃&body=Buenas: \nTengo una sugerencia para la app Gaztaroa')
```

**Video:**

![Envío de correos - Redirección de links web - Apertura de número de telefono](./videos/4.%20Env%C3%ADo%20de%20correos%20-%20Redirecci%C3%B3n%20de%20links%20web%20-%20Apertura%20de%20n%C3%BAmero%20de%20telefono.mp4)
<figure class="video_container">
  <video width=300 controls="true" allowfullscreen="true" poster="./images/4. Envío de correos - Redirección de links web - Apertura de número de telefono.jpg">
    <source src="https://gitlab.com/asiier/ejercicios-react-native-dsm/-/raw/main/docs/videos/4.%20Env%C3%ADo%20de%20correos%20-%20Redirecci%C3%B3n%20de%20links%20web%20-%20Apertura%20de%20n%C3%BAmero%20de%20telefono.mp4" type="video/mp4">
  </video>
</figure>
